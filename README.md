# MDT: Markdown for TechNotes

TechNotes contains many "entries" and "articles" which support rich text descriptions and content. The rich text descriptions and content are authored and represented using "MDT" (MarkDown for Technotes), which is a superset of [CommonMark](https://commonmark.org/) (markdown), with additional TechNotes features added.

This repository contains a parser for MDT, based on [markdown-it](https://github.com/markdown-it/markdown-it).

## Inline vs. article mode

Every distinct place where MDT is used is in either "inline" or "article" mode. In inline mode, no paragraphs, images, or other block-level elements are allowed, and a reduced set of features is available. In "article" mode, the full MDT feature set is available.

## Differences with CommonMark

MDT is based on [CommonMark](https://commonmark.org/), with some changes and additional features:

### No inline images

The standard [CommonMark image syntax](https://spec.commonmark.org/0.29/#images) (`![description](/url)`) and its variants is not supported. MDT will add support for block-level images only.

### Limited HTML Support

In addition to HTML entities like `&amp;`, only the following tags can be used:

* `<sup>superscript</sup>`
* `<sub>subscript</sub>`

Any other HTML tags will be escaped.

### Cannot specify link titles

The `[link](/url "title")` syntax for specifying a link title is not supported. TechNotes generates the title automatically based on what article is linked to.
