import { Node, InlineNode, RootNode, AnyInlineNode, AnyBlockNode } from "./mdt-node";
export { Node, InlineNode, RootNode, AnyInlineNode, AnyBlockNode };
export interface Options {
    inline?: boolean;
}
export declare function tokenizeMDT(mdtString: string, options?: Options): RootNode;
export declare function tokenizeInlineMDT(mdtString: string): InlineNode;
export declare function renderInlineToHTML(inlineNode: Node): string;
/**
 * Render MDT to HTML, ignoring block-level elements.
 * @param mdt The MDT string to parse and convert to HTML
 */
export declare function renderMDTInlineToHTML(mdt: string): string;
