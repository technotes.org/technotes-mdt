"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubPlugin = void 0;
/**
 * markdown-it plugin that allows <sub> and <sup> tags in the Markdown
 */
function SubPlugin(md) {
    const tokenize = (state, silent) => {
        const { pos, posMax } = state;
        // Is this the start of an HTML <tag> ?
        if (state.src.charCodeAt(pos) !== 60 /* LessThan */) {
            return false;
        }
        // Is this a <sub> or <sup> opening tag or a </sub> or </sup> closing tag?
        const isClosing = (state.src.charCodeAt(pos + 1) === 47 /* Slash */); // Note this may be out of bounds but that's OK
        const tagLength = isClosing ? 6 : 5; // Length of "<su_>" or "</su_>" that we may be parsing
        // Is there enough room for the <sub>, </sub>, <sup>, or </sup> that we may be parsing now?
        if (pos + tagLength > posMax) {
            return false;
        }
        // Index of the 's' in <sub>,<sup>,</sup>,</sub>
        const sPos = pos + (isClosing ? 2 : 1);
        // Check that the next few chars are "sub>" or "sup>"
        if (state.src.charCodeAt(sPos) !== 115 /* s */ && state.src.charCodeAt(sPos) !== 83 /* S */) {
            return false;
        }
        if (state.src.charCodeAt(sPos + 1) !== 117 /* u */ && state.src.charCodeAt(sPos + 1) !== 85 /* U */) {
            return false;
        }
        const isSub = (state.src.charCodeAt(sPos + 2) === 98 /* b */ || state.src.charCodeAt(sPos + 2) === 66 /* B */);
        const isSup = (state.src.charCodeAt(sPos + 2) === 112 /* p */ || state.src.charCodeAt(sPos + 2) === 80 /* P */);
        if (!isSub && !isSup) {
            return false;
        }
        if (state.src.charCodeAt(sPos + 3) !== 62 /* GreaterThan */) {
            return false;
        }
        // Validate that we're not already in a <sub> or <sup>, or that we are if isClosing
        const tag = isSub ? "sub" : "sup";
        const startType = isSub ? "sub_open" /* StartSub */ : "sup_open" /* StartSup */;
        const endType = isSub ? "sub_close" /* EndSub */ : "sup_close" /* EndSup */;
        let lastToken;
        for (let i = state.tokens.length - 1; i > 0; i--) {
            if (state.tokens[i].type === "sup_open" /* StartSup */ ||
                state.tokens[i].type === "sub_open" /* StartSub */ ||
                state.tokens[i].type === "sup_close" /* EndSup */ ||
                state.tokens[i].type === "sub_close" /* EndSub */) {
                lastToken = state.tokens[i];
                break;
            }
        }
        if (isClosing) {
            // If this is a closing tag, it must have a matching start tag
            if (lastToken === undefined || lastToken.type !== startType) {
                return false;
            }
        }
        else {
            // If this is an opening tag, the last tag must not be a start tag
            if (lastToken !== undefined && (lastToken.type === "sup_open" /* StartSup */ || lastToken.type === "sub_open" /* StartSub */)) {
                return false;
            }
            // And we must look ahead to ensure there's a closing tag.
            if (state.src.slice(sPos, posMax).search(new RegExp(`[^\\\\]</${tag}>`, "i")) === -1) {
                return false;
            }
        }
        if (!silent) {
            if (isClosing) {
                state.push(endType, tag, -1);
            }
            else {
                state.push(startType, tag, 1);
            }
        }
        state.pos += tagLength;
        return true;
    };
    md.inline.ruler.push("subsup", tokenize);
}
exports.SubPlugin = SubPlugin;
