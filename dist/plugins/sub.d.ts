import MarkdownIt from "markdown-it";
/**
 * markdown-it plugin that allows <sub> and <sup> tags in the Markdown
 */
export declare function SubPlugin(md: MarkdownIt): void;
