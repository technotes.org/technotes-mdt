"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderMDTInlineToHTML = exports.renderInlineToHTML = exports.tokenizeInlineMDT = exports.tokenizeMDT = void 0;
const markdown_it_1 = __importDefault(require("markdown-it"));
const utils_1 = require("markdown-it/lib/common/utils");
const sub_1 = require("./plugins/sub");
const parser = markdown_it_1.default("commonmark", {
    breaks: false,
    html: false,
    linkify: false,
    typographer: false,
    xhtmlOut: false,
})
    .disable("image") // Disable inline images
    .enable("strikethrough") // Enable ~~strikethrough~~
    .enable("table") // Enable tables (GitHub-style)
    .use(sub_1.SubPlugin); // Allow use of <sub> and <sup> tags
function tokenizeMDT(mdtString, options = {}) {
    const env = {};
    const tokens = options.inline ? parser.parseInline(mdtString, env) : parser.parse(mdtString, env);
    return buildAST(tokens);
}
exports.tokenizeMDT = tokenizeMDT;
function tokenizeInlineMDT(mdtString) {
    const root = tokenizeMDT(mdtString, { inline: true });
    if (root.children.length !== 1) {
        throw new Error(`Expected a single inline node from MDT document.`);
    }
    const childNode = root.children[0];
    if (childNode.type !== "inline") {
        throw new Error(`Expected a single inline node from MDT document, got ${childNode.type}.`);
    }
    return childNode;
}
exports.tokenizeInlineMDT = tokenizeInlineMDT;
/**
 * Compile the Token stream produced by markdown-it's parser into a more useful tree
 */
function buildAST(tokens) {
    const root = {
        type: "mdt-document",
        children: [],
    };
    let currNode = root;
    const stack = [];
    const addChild = (node) => {
        var _a;
        if (currNode.children === undefined) {
            throw new Error(`Tried to add child ${JSON.stringify(node)} to node ${JSON.stringify(currNode)} which is missing .children = []`);
        }
        (_a = currNode.children) === null || _a === void 0 ? void 0 : _a.push(node);
    };
    const pushTokens = (token) => {
        var _a;
        if (token.hidden) {
            return; // Ignore "hidden" tokens, i.e. paragraphs that wrap list items
        }
        else if (token.type === "text" && token.content === "") {
            return; // Special case: elide empty text nodes.
        }
        const type = getTokenType(token);
        if (type === "inline") {
            const inlineNode = tokenToNode(token);
            addChild(inlineNode);
            stack.push(currNode);
            currNode = inlineNode;
            (_a = token.children) === null || _a === void 0 ? void 0 : _a.forEach(pushTokens);
            const poppedNode = stack.pop();
            if (!poppedNode) {
                throw new Error("AST stack underflow.");
            }
            currNode = poppedNode;
        }
        else if (token.nesting == 1) {
            const child = tokenToNode(token);
            addChild(child);
            stack.push(currNode);
            currNode = child;
        }
        else if (token.nesting == -1) {
            const poppedNode = stack.pop();
            if (!poppedNode) {
                throw new Error("AST stack underflow.");
            }
            if (type !== currNode.type) {
                throw new Error(`Token mismatch: got ${type} but was expecting ${currNode.type}_close`);
            }
            currNode = poppedNode;
        }
        else if (token.nesting == 0) {
            const node = tokenToNode(token);
            addChild(node);
        }
        else {
            throw new Error(`Invalid nesting level found in AST token.`);
        }
    };
    tokens.forEach(pushTokens);
    if (stack.length !== 0) {
        throw new Error("MDT Parse Error: Unbalanced block open/close tokens");
    }
    return root;
}
function getTokenType(token) {
    let type;
    if (token.nesting === 1 && token.type.endsWith("_open")) {
        type = token.type.slice(0, -5);
    }
    else if (token.nesting === -1 && token.type.endsWith("_close")) {
        type = token.type.slice(0, -6);
    }
    else {
        type = token.type;
    }
    // Conversions:
    if (type === "fence") {
        type = "code_block";
    }
    return type;
}
function tokenToNode(token) {
    var _a;
    const type = getTokenType(token);
    const node = {
        type,
    };
    // Block attribute:
    if (token.block && node.type !== "inline") {
        node.block = true;
    }
    // Content/children attributes:
    if (type === "text" || type === "code_inline" || type === "code_block") {
        // This node contains text content:
        node.content = token.content;
    }
    else {
        // Otherwise it might contain child nodes:
        if (token.content) {
            // Just check to make sure we're not forgetting some node type that has content:
            if (type === "inline") {
                // Ignore content on inline token - its child tokens have all the same content.
            }
            else {
                throw new Error(`Unexpected content: ${JSON.stringify(token)}`);
            }
        }
        // Create "children" attribute for this node:
        if (type !== "softbreak" && type !== "hardbreak" && type !== "hr") {
            node.children = [];
        }
    }
    // Special case handling:
    if (type === "link") {
        const href = (_a = token.attrGet("href")) !== null && _a !== void 0 ? _a : "";
        node.href = href;
    }
    else if (type === "heading") {
        // Determine the heading level from the tag, e.g. "h2" -> 2
        node.level = parseInt(token.tag.slice(1), 10);
    }
    else if (type === "ordered_list") {
        const listStart = token.attrGet("start");
        if (listStart) {
            node.start = parseInt(listStart, 10);
        }
    }
    else if (type === "td" || type === "th") {
        const style = token.attrGet("style");
        if (style === "text-align:right") {
            node.align = "right";
        }
        else if (style === "text-align:center") {
            node.align = "center";
        }
    }
    return node;
}
function renderInlineToHTML(inlineNode) {
    if (inlineNode.type !== "inline") {
        throw new Error(`renderInlineToHTML() can only render inline nodes to HTML, not ${inlineNode}`);
    }
    let html = "";
    const renderNode = (childNode) => {
        if (childNode.type === "text") {
            html += utils_1.escapeHtml(childNode.content);
            return;
        }
        else if (childNode.type === "code_inline") {
            html += "<code>" + utils_1.escapeHtml(childNode.content) + "</code>";
            return;
        }
        else if (childNode.type === "softbreak") {
            html += "\n";
            return;
        }
        else if (childNode.type === "hardbreak") {
            html += "<br>";
            return;
        }
        let start = "", end = "";
        switch (childNode.type) {
            case "strong":
            case "em":
            case "sup":
            case "sub":
            case "s": {
                start = `<${childNode.type}>`, end = `</${childNode.type}>`;
                break;
            }
            case "link": {
                start = `<a href="${childNode.href}">`;
                end = "</a>";
                break;
            }
        }
        html += start;
        // if ("children" in childNode) {
        childNode.children.forEach(renderNode);
        // }
        html += end;
    };
    inlineNode.children.forEach(renderNode);
    return html;
}
exports.renderInlineToHTML = renderInlineToHTML;
/**
 * Render MDT to HTML, ignoring block-level elements.
 * @param mdt The MDT string to parse and convert to HTML
 */
function renderMDTInlineToHTML(mdt) {
    const document = tokenizeMDT(mdt, { inline: true });
    return document.children.map(node => {
        if (node.type !== "inline") {
            throw new Error(`Unexpected node type ${node.type} when parsing MDT as inline-only.`);
        }
        return renderInlineToHTML(node);
    }).join("");
}
exports.renderMDTInlineToHTML = renderMDTInlineToHTML;
