# MDT demo and test case

MDT is **M**ark**D**own for [**T**echNotes](https://technotes.org).

## Blockquotes

> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.

## Lists

Unordered

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa

1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`

Start numbering with offset:

57. foo
1. bar


## Code

Inline `code`

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code
    can include <html>


Block code "fences"

```
Sample text here...
```

### Horizontal rule

---------------------------

Link using references: [ref link], and again: [ref link]

[ref link]: /url

## Table

| Letter    | Description (right aligned) |
| --------- | ------------:|
| Alpha (Α) | First letter |
| Omega (Ω) | Last letter  |
