import {readFile} from "fs/promises";
import intern from "intern";
import { Node, tokenizeMDT } from "../src";

const { suite, test, before, beforeEach, after, afterEach } = intern.getPlugin("interface.tdd");
const { assert } = intern.getPlugin("chai");

// Simplify the output and make it more readable by collapsing a common case: an inline node containing only text.
function inlineText(content: string): Node { 
    return {
        type: "inline",
        children: [
            {type: "text", content},
        ],
    };
}

suite ("MDT full demo", () => {
    
    test("tokenize full demo MDT file", async () => {

        // Demo text based on the demo at https://markdown-it.github.io/
        
        const mdt = await readFile(`${__dirname}/full-demo.md`, {encoding: "utf8"});
        
        const document = tokenizeMDT(mdt);
        
        assert.deepStrictEqual(document, {
            type: "mdt-document",
            children: [
                // # MDT demo and test case
                {
                    type: "heading",
                    level: 1,
                    block: true,
                    children: [
                        inlineText("MDT demo and test case"),
                    ],
                },
                // MDT is **M**ark**D**own for [**T**echNotes](https://technotes.org).
                {
                    type: "paragraph",
                    block: true,
                    children: [
                        {
                            type: "inline",
                            children: [
                                {type: "text", content: "MDT is "},
                                {type: "strong", children: [ {type: "text", content: "M"} ]},
                                {type: "text", content: "ark"},
                                {type: "strong", children: [ {type: "text", content: "D"} ]},
                                {type: "text", content: "own for "},
                                {
                                    type: "link",
                                    "href": "https://technotes.org",
                                    children: [
                                        {type: "strong", children: [ {type: "text", content: "T"} ]},
                                        {type: "text", content: "echNotes"}
                                    ],
                                },
                                {type: "text", content: "."},
                            ],
                        }
                    ],
                },
                // ## Blockquotes
                {
                    type: "heading",
                    level: 2,
                    block: true,
                    children: [
                        inlineText("Blockquotes"),
                    ],
                },
                // > Blockquotes can also be nested...
                // >> ...by using additional greater-than signs right next to each other...
                // > > > ...or with spaces between arrows.
                {
                    type: "blockquote",
                    block: true,
                    children: [
                        {
                            type: "paragraph",
                            block: true,
                            children: [
                                inlineText("Blockquotes can also be nested...")
                            ],
                        },
                        {
                            type: "blockquote",
                            block: true,
                            children: [
                                {
                                    type: "paragraph",
                                    block: true,
                                    children: [
                                        inlineText("...by using additional greater-than signs right next to each other..."),
                                    ],
                                },
                                {
                                    type: "blockquote",
                                    block: true,
                                    children: [
                                        {
                                            type: "paragraph",
                                            block: true,
                                            children: [
                                                inlineText("...or with spaces between arrows.")
                                            ],
                                        }
                                    ],
                                }
                            ],
                        }
                    ],
                },
                // ## Lists
                //
                // Unordered
                {
                    type: "heading",
                    level: 2,
                    block: true,
                    children: [ inlineText("Lists") ],
                },
                {
                    type: "paragraph",
                    block: true,
                    children: [ inlineText("Unordered") ],
                },
                // + Create a list by starting a line with `+`, `-`, or `*`
                // + Sub-lists are made by indenting 2 spaces:
                // - Marker character change forces new list start:
                //     * Ac tristique libero volutpat at
                //     + Facilisis in pretium nisl aliquet
                //     - Nulla volutpat aliquam velit
                // + Very easy!
                {
                    type: "bullet_list",
                    block: true,
                    children: [
                        {
                            type: "list_item",
                            block: true,
                            children: [
                                {
                                    type: "inline",
                                    children: [
                                        {type: "text", content: "Create a list by starting a line with "},
                                        {type: "code_inline", content: "+"},
                                        {type: "text", content: ", "},
                                        {type: "code_inline", content: "-"},
                                        {type: "text", content: ", or "},
                                        {type: "code_inline", content: "*"},
                                    ],
                                }
                            ],
                        },
                        {
                            type: "list_item",
                            block: true,
                            children: [
                                inlineText("Sub-lists are made by indenting 2 spaces:"),
                                {
                                    type: "bullet_list",
                                    block: true,
                                    children: [
                                        {
                                            type: "list_item",
                                            block: true,
                                            children: [
                                                inlineText("Marker character change forces new list start:"),
                                                {
                                                    type: "bullet_list",
                                                    block: true,
                                                    children: [
                                                        {
                                                            type: "list_item",
                                                            block: true,
                                                            children: [inlineText("Ac tristique libero volutpat at")],
                                                        }
                                                    ],
                                                },
                                                {
                                                    type: "bullet_list",
                                                    block: true,
                                                    children: [
                                                        {
                                                            type: "list_item",
                                                            block: true,
                                                            children: [ inlineText("Facilisis in pretium nisl aliquet") ],
                                                        }
                                                    ],
                                                },
                                                {
                                                    type: "bullet_list",
                                                    block: true,
                                                    children: [
                                                        {
                                                            type: "list_item",
                                                            block: true,
                                                            children: [inlineText("Nulla volutpat aliquam velit")],
                                                        }
                                                    ],
                                                }
                                            ],
                                        }
                                    ],
                                }
                            ],
                        },
                        {
                            type: "list_item",
                            block: true,
                            children: [inlineText("Very easy!")],
                        }
                    ],
                },
                // Ordered
                {
                    type: "paragraph",
                    block: true,
                    children: [ inlineText("Ordered") ],
                },
                // 1. Lorem ipsum dolor sit amet
                // 2. Consectetur adipiscing elit
                // 3. Integer molestie lorem at massa
                //
                // 1. You can use sequential numbers...
                // 1. ...or keep all the numbers as `1.`
                {
                    type: "ordered_list",
                    block: true,
                    children: [
                        {
                            type: "list_item",
                            block: true,
                            children: [
                                {
                                    type: "paragraph",
                                    block: true,
                                    children: [inlineText("Lorem ipsum dolor sit amet")],
                                },
                            ],
                        },
                        {
                            type: "list_item",
                            block: true,
                            children: [
                                {
                                    type: "paragraph",
                                    block: true,
                                    children: [inlineText("Consectetur adipiscing elit")],
                                }
                            ],
                        },
                        {
                            type: "list_item",
                            block: true,
                            children: [
                                {
                                    type: "paragraph",
                                    block: true,
                                    children: [inlineText("Integer molestie lorem at massa")],
                                }
                            ],
                        },
                        {
                            type: "list_item",
                            block: true,
                            children: [
                                {
                                    type: "paragraph",
                                    block: true,
                                    children: [inlineText("You can use sequential numbers...")],
                                }
                            ],
                        },
                        {
                            type: "list_item",
                            block: true,
                            children: [
                                {
                                    block: true,
                                    children: [
                                        {
                                            type: "inline",
                                            children: [
                                                {type: "text", content: "...or keep all the numbers as "},
                                                {type: "code_inline", content: "1."},
                                            ],
                                        }
                                    ],
                                    type: "paragraph",
                                }
                            ],
                        },
                    ],
                },
                // Start numbering with offset:
                {
                    block: true,
                    children: [inlineText("Start numbering with offset:")],
                    type: "paragraph",
                },
                // 57. foo
                // 1. bar
                {
                    type: "ordered_list",
                    start: 57,
                    block: true,
                    children: [
                        {
                            type: "list_item",
                            block: true,
                            children: [inlineText("foo")],
                        },
                        {
                            type: "list_item",
                            block: true,
                            children: [inlineText("bar")],
                        }
                    ],
                },
                // ## Code
                {
                    type: "heading",
                    level: 2,
                    block: true,
                    children: [ inlineText("Code") ],
                },
                // Inline `code`
                {
                    type: "paragraph",
                    block: true,
                    children: [{
                        type: "inline",
                        children: [
                            {type: "text", content: "Inline "},
                            {type: "code_inline", content: "code"},
                        ],
                    }],
                },
                // Indented code
                //
                //     // Some comments
                //     line 1 of code
                //     line 2 of code
                //     line 3 of code
                //     can include <html>
                {
                    type: "paragraph",
                    block: true,
                    children: [inlineText("Indented code")],
                },
                {
                    type: "code_block",
                    block: true,
                    content: "// Some comments\nline 1 of code\nline 2 of code\nline 3 of code\ncan include <html>\n",
                },
                // Block code "fences"
                //
                // ```
                // Sample text here...
                // ```
                {
                    type: "paragraph",
                    block: true,
                    children: [ inlineText("Block code \"fences\"") ],
                },
                {
                    type: "code_block",
                    block: true,
                    content: "Sample text here...\n",
                },
                // Horizontal rule
                //
                // ---------------------------
                {
                    type: "heading",
                    level: 3,
                    block: true,
                    children: [ inlineText("Horizontal rule") ],
                },
                {
                    type: "hr",
                    block: true,
                },
                // Link using references: [ref link], and again: [ref link]
                //
                // [ref link]: /url
                {
                    type: "paragraph",
                    block: true,
                    children: [
                        {
                            type: "inline",
                            children: [
                                {type: "text", content: "Link using references: "},
                                {type: "link", href: "/url", children: [ {type: "text", content: "ref link"} ]},
                                {type: "text", content: ", and again: "},
                                {type: "link", href: "/url", children: [ {type: "text", content: "ref link"} ]},
                            ],
                        }
                    ],
                },
                // ## Table
                //
                // | Letter    | Description (right aligned) |
                // | --------- | ------------:|
                // | Alpha (Α) | First letter |
                // | Omega (Ω) | Last letter  |
                {
                    type: "heading",
                    level: 2,
                    block: true,
                    children: [ inlineText("Table") ],
                },
                {
                    type: "table",
                    block: true,
                    children: [
                        {
                            type: "thead",
                            block: true,
                            children: [
                                {
                                    type: "tr",
                                    block: true,
                                    children: [
                                        {type: "th", block: true, children: [inlineText("Letter")]},
                                        {type: "th", block: true, children: [inlineText("Description (right aligned)")], align: "right"},
                                    ],
                                },
                            ],
                        },
                        {
                            type: "tbody",
                            block: true,
                            children: [
                                {
                                    type: "tr",
                                    block: true,
                                    children: [
                                        {type: "td", block: true, children: [inlineText("Alpha (Α)")]},
                                        {type: "td", block: true, children: [inlineText("First letter")], align: "right"},
                                    ],
                                },
                                {
                                    type: "tr",
                                    block: true,
                                    children: [
                                        {type: "td", block: true, children: [inlineText("Omega (Ω)")]},
                                        {type: "td", block: true, children: [inlineText("Last letter")], align: "right"},
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        });
    });
});
