import intern from "intern";
import { tokenizeMDT, Node, InlineNode, renderMDTInlineToHTML, tokenizeInlineMDT } from "../src";

const { suite, test } = intern.getPlugin("interface.tdd");
const { assert } = intern.getPlugin("chai");

function assertInline(src: string, result: InlineNode) {
    const inlineTree = tokenizeInlineMDT(src);
    assert.deepStrictEqual(inlineTree, result);
}

function assertArticle(src: string, result: Node[]) {
    const articleTree = tokenizeMDT(src, {inline: false});
    assert.deepStrictEqual(articleTree, {
        type: "mdt-document",
        children: result,
    });
}

const check = (mdt: string, htmlExpected: string): void => {
    const htmlActual = renderMDTInlineToHTML(mdt);
    assert.equal(htmlActual, htmlExpected);
};

suite("MDT Test Suite", () => {

    suite("simple tests", () => {

        test("simple text with inline formatting", () => {
            const src = "This has some **bold text** and *italic text*.";

            const expectedInline: Node = {
                type: "inline",
                children: [
                    {type: "text", content: "This has some "},
                    {type: "strong", children: [{type: "text", content: "bold text"}]},
                    {type: "text", content: " and "},
                    {type: "em", children: [{type: "text", content: "italic text"}]},
                    {type: "text", content: "."},
                ],
            };

            assertInline(src, expectedInline);

            assertArticle(src, [
                {
                    type: "paragraph",
                    block: true,
                    children: [
                        expectedInline,
                    ],
                },
            ]);
        });

        test("simple description with links", () => {
            const src = "A collection of [PV modules](t-pv-module) connected together is called a [**PV array**](t-pv-array), and an [array](t-pv-array) is usually part of a [photovoltaic system](t-pv-system)";

            const expectedInline: Node = {
                type: "inline",
                children: [
                    {type: "text", content: "A collection of "},
                    {type: "link", href: "t-pv-module", children: [{type: "text", content: "PV modules"}]},
                    {type: "text", content: " connected together is called a "},
                    {type: "link", href: "t-pv-array", children: [
                        {type: "strong", children: [{type: "text", content: "PV array"}]},
                    ]},
                    {type: "text", content: ", and an "},
                    {type: "link", href: "t-pv-array", children: [{type: "text", content: "array"}]},
                    {type: "text", content: " is usually part of a "},
                    {type: "link", href: "t-pv-system", children: [{type: "text", content: "photovoltaic system"}]},
                ],
            };

            assertInline(src, expectedInline);

            assertArticle(src, [
                {
                    type: "paragraph",
                    block: true,
                    children: [
                        expectedInline,
                    ],
                },
            ]);
        });

        test("Blocks parsed in inline mode are ignored", () => {
            const src = "# Header\n\nParagraph\n\n";

            const expectedInline: Node = {
                type: "inline",
                children: [
                    {type: "text", content: "# Header"},
                    {type: "softbreak"},
                    {type: "softbreak"},
                    {type: "text", content: "Paragraph"},
                    {type: "softbreak"},
                    {type: "softbreak"},
                ],
            };
            assertInline(src, expectedInline);
            check(src, "# Header\n\nParagraph\n\n");
        });

        test("Hard line break", () => {
            const src = "Lines ending with\\\nor two spaces:  \ngive hard line breaks.";

            const expectedInline: Node = {
                type: "inline",
                children: [
                    {type: "text", content: "Lines ending with"},
                    {type: "hardbreak"},
                    {type: "text", content: "or two spaces:"},  // Notice the two spaces are now removed
                    {type: "hardbreak"},
                    {type: "text", content: "give hard line breaks."},
                ],
            };
            assertInline(src, expectedInline);
            check(src, "Lines ending with<br>or two spaces:<br>give hard line breaks.");
        });

        test("HTML entity references and numeric character references", () => {
            // https://spec.commonmark.org/0.29/#entity-and-numeric-character-references
            const src = "Special characters: &frac34; &HilbertSpace; &copy; &ClockwiseContourIntegral; &amp;";

            const expectedInline: Node = {
                type: "inline",
                children: [
                    {type: "text", content: "Special characters: ¾ ℋ © ∲ &"},
                ],
            };
            assertInline(src, expectedInline);
            check(src, "Special characters: ¾ ℋ © ∲ &amp;");

            const src2 = "But these are not: &nonentity; &amp";
            check(src2, "But these are not: &amp;nonentity; &amp;amp");
        });

        test("strikethrough", () => {
            check("This is ~~struck out~~.", "This is <s>struck out</s>.");
        });

        test("Inline code", () => {
            const src = "Here we have some `inline <code>`.";

            const expectedInline: Node = {
                type: "inline",
                children: [
                    {type: "text", content: "Here we have some "},
                    {type: "code_inline", content: "inline <code>"},
                    {type: "text", content: "."},
                ],
            };
            assertInline(src, expectedInline);
            check(src, "Here we have some <code>inline &lt;code&gt;</code>.");
        });

        test("autolink", () => {
            // https://spec.commonmark.org/0.29/#autolinks
            check(
                "This is a link: <http://foo.bar.baz>",
                `This is a link: <a href="http://foo.bar.baz">http://foo.bar.baz</a>`,
            );
        });

        test("URLs are not automatically linked", () => {
            check(
                "Here is http://www.technotes.org a URL.",
                "Here is http://www.technotes.org a URL.",
            );

            check(
                "Here is someone@example.com an email.",
                "Here is someone@example.com an email.",
            );
        });

        test("Supports Unicode", () => {
            check(
                "**Bold emojis: 🔬🔥**, and greek: Σωκρᾰ́της",
                "<strong>Bold emojis: 🔬🔥</strong>, and greek: Σωκρᾰ́της",
            );
        })
    });

    suite("HTML Escaping", () => {
        const src = "This has some <strong>HTML</strong> <img src='foo' onload='danger();'>";

        test("Parsed AST does not escape HTML", () => {
            // In order to safely use this with React etc., we need the HTML to be unescaped:
            assertInline(src, {
                type: "inline",
                children: [
                    {type: "text", content: "This has some <strong>HTML</strong> <img src='foo' onload='danger();'>"},
                ],
            });
        });

        test("renderMDTInlineToHTML() does escape HTML", () => {
            // But when using the renderMDTInlineToHTML helper method which returns an HTML string, HTML should be escaped:

            check(src, "This has some &lt;strong&gt;HTML&lt;/strong&gt; &lt;img src='foo' onload='danger();'&gt;");
        });

        const src2 = "This links are sus: [one](javascript:window.something), [two](\"><strong>HTML</strong>)";

        test("renderMDTInlineToHTML() escapes dangerous link attributes", () => {
            check(src2, `This links are sus: [one](javascript:window.something), <a href="%22%3E%3Cstrong%3EHTML%3C/strong%3E">two</a>`);
        });
    });


    suite("Differences from CommonMark", () => {

        test("No inline images", () => {
            // We don't want images appearing in article descriptions or inline in sentences in the middle of paragraphs.
            check(
                `Not an image: ![description](/img.png)`,
                `Not an image: !<a href="/img.png">description</a>`,
            );
        })

        test("Cannot specify link titles", () => {
            // TechNotes auto-generates link titles, so for consistency they are not allowed to be overridden.
            check(
                `[This link](/url "title") has a title.`,
                `<a href="/url">This link</a> has a title.`,
            );
        });
    });

    suite("Superscript and subscript plugin", () => {

        test("supports <sub> and <sup>", () => {
            check(
                "e = mc<sup>2</sup> and H<sub>2</sub>O",
                "e = mc<sup>2</sup> and H<sub>2</sub>O",
            );
        });

        test("is case insensitive", () => {
            check(
                "e = mc<SUP>2</sup> and H<sub>2</SUB>O",
                "e = mc<sup>2</sup> and H<sub>2</sub>O",
            );
        });

        test("ignores other tags", () => {
            check(
                "e = mc<sud>2</sud> and H<bus>2</bus>O",
                "e = mc&lt;sud&gt;2&lt;/sud&gt; and H&lt;bus&gt;2&lt;/bus&gt;O",
            );
        });

        test("ignores <sup> inside a <sub>", () => {
            check(
                "blah <sub>outer <sup>inner</sup> outer</sub> blah",
                "blah <sub>outer &lt;sup&gt;inner&lt;/sup&gt; outer</sub> blah",
            );
        });

        test("ignores <sub> inside a <sup>", () => {
            check(
                "blah <sup>outer <sub>inner</sub> outer</sup> blah",
                "blah <sup>outer &lt;sub&gt;inner&lt;/sub&gt; outer</sup> blah",
            );
        });

        test("ignores lone <sub> or <sup> without closing tag", () => {
            check(
                "blah <sup>outer blah",
                "blah &lt;sup&gt;outer blah",
            );
            check(
                "blah <sub>outer blah</other> blah",
                "blah &lt;sub&gt;outer blah&lt;/other&gt; blah",
            );
        });

        test("can be escaped", () => {
            check(
                "This is \\<sub>NOT</sub> subscript.",
                "This is &lt;sub&gt;NOT&lt;/sub&gt; subscript.",
            );

            check(
                "This is <sub>not\\</sub> subscript.",
                "This is &lt;sub&gt;not&lt;/sub&gt; subscript.",
            );
        });

    });
});
